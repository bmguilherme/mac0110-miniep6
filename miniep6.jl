# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)

function impares_consecutivos(n)
        i = n * (n - 1) + 1
        return i
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function suporte_imprime_impares_consecutivos(m)
        i = m * (m - 1) + 1
        j = 0
        for k in 0:(m - 1)
                j = i + 2(k)
                print(" ", j)
        end
end

function imprime_impares_consecutivos(m)
        print(" ", m, " ", (m ^ 3))   
        repr(suporte_imprime_impares_consecutivos(m))
end

function mostra_n(n)
        for m in 1:n
                l = imprime_impares_consecutivos(m)
                println(" ", l)
        end
end

# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
    println("Fim dos testes :)")
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
test()
